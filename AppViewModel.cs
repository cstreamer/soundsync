﻿using CStreamer;
using System.Windows;
using System.Windows.Input;

namespace SoundSync
{
    class AppViewModel : ViewModel
    {
        public AudioViewModel Audio { get; set; }
        public ServerViewModel Server { get; set; }

        private readonly PipeLine pipeLine;

        private bool _Connected;

        public bool Connected
        {
            get { return _Connected; }
            set { 
                _Connected = value;
                this.Audio.Connected = value;
                this.Server.Connected = value;
                this.OnPropertyChanged();
            }
        }

        private State state = State.Stopped;

        public State State
        {
            get { return state; }
        }

        public Visibility ShowPlay
        {
            get
            {
                return this.State != State.Playing ? Visibility.Visible: Visibility.Collapsed;
            }
        }

        public Visibility ShowPause
        {
            get
            {
                return this.State == State.Playing? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ShowStop
        {
            get
            {
                return this.State != State.Stopped ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public ICommand Play { get; }
        public ICommand Pause { get; }
        public ICommand Stop { get; }


        private async void DoPlay(object _)
        {
            var state = State.Playing;
            await ChangeState(state);
        }

        private async void DoPause(object _)
        {
            var state = State.Ready;
            await ChangeState(state);
        }

        private async void DoStop(object _)
        {
            var state = State.Stopped;
            await ChangeState(state);
        }

        private async System.Threading.Tasks.Task ChangeState(State state)
        {
            var result = await this.pipeLine.GoToState(state).ConfigureAwait(true);

            result.MatchSome((value) =>
            {
                this.state = value;
                this.Connected = value != State.Stopped;
                OnPropertyChanged(nameof(ShowPlay));
                OnPropertyChanged(nameof(ShowPause));
                OnPropertyChanged(nameof(ShowStop));
            });
        }

        public AppViewModel(AudioViewModel audio, ServerViewModel server, PipeLine pipeLine)
        {
            this.Audio = audio;
            this.Server = server;

            this.pipeLine = pipeLine;

            this.Play = new DelegateCommand<object>(DoPlay);
            this.Pause = new DelegateCommand<object>(DoPause);
            this.Stop = new DelegateCommand<object>(DoStop);
        }
    }
}
