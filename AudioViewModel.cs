﻿using System;
using System.Collections.Generic;
using System.Text;
using CStreamer.Plugins.Basic;
using CStreamer.Plugins.NAudio;

namespace SoundSync
{
    class AudioViewModel : ViewModel
    {
        private readonly LoopBackSrc src;
        private readonly TimedAVGElement timedAvg;
        private readonly MultiplyElement multiply;

        private bool _Connected;
        public bool Connected
        {
            get { return _Connected; }
            set
            {
                _Connected = value;
                this.OnPropertyChanged();
            }
        }

        public int UpdateInterval
        {
            get { return this.timedAvg.AVGMs; }
            set {
                this.timedAvg.AVGMs = value;
                this.OnPropertyChanged();
            }
        }

        public double Multiplier
        {
            get
            {
                return this.multiply.Multiplier;
            }
            set
            {
                this.multiply.Multiplier = value;
                this.OnPropertyChanged();
            }
        }

        public AudioViewModel(LoopBackSrc src, TimedAVGElement timedAvg, MultiplyElement multiply)
        {
            this.src = src;
            this.timedAvg = timedAvg;
            this.multiply = multiply;
        }
    }
}
