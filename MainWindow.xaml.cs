﻿using CStreamer;
using CStreamer.Plugins.Basic;
using CStreamer.Plugins.Buttplug;
using CStreamer.Plugins.NAudio;
using System.Windows;

namespace SoundSync
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly AppViewModel viewModel;

        public MainWindow()
        {
            var pipeLine = new PipeLine();

            var source = new LoopBackSrc();
            var enumerate = new EnumerateElement();
            var convert = new ConvertElement();
            var timedAvg = new TimedAVGElement();
            var multiply = new MultiplyElement();
            var sink = new ButtplugSink();

            timedAvg.AVGMs = 100;
            multiply.Multiplier = 10;
            sink.ServerAddress = "ws://localhost:12345/buttplug";

            pipeLine.Add(source);
            pipeLine.Add(enumerate);
            pipeLine.Add(convert);
            pipeLine.Add(timedAvg);
            pipeLine.Add(multiply);
            pipeLine.Add(sink);

            pipeLine.Connect(source.Src, enumerate.Sink);
            if (!(pipeLine.TryConnect(enumerate.Src, convert.Sink) && pipeLine.TryConnect(convert.Src, timedAvg.Sink)))
            {
                return;
            }
            pipeLine.Connect(timedAvg.Src, multiply.Sink);
            pipeLine.Connect(multiply.Src, sink.Sink);

            this.viewModel = new AppViewModel(new AudioViewModel(source, timedAvg, multiply), new ServerViewModel(sink), pipeLine);
            this.DataContext = this.viewModel;

            InitializeComponent();

        }
    }
}
