﻿using CStreamer.Plugins.Buttplug;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Text;
using System.Windows;
using System.Windows.Input;

namespace SoundSync
{
    class ServerViewModel : ViewModel
    {
        private readonly ButtplugSink sink;

        public ServerViewModel(ButtplugSink sink)
        {
            this.sink = sink;

            this.StartScanningCommand = new DelegateCommand<object>(StartScanning);
            this.StopScanningCommand = new DelegateCommand<object>(StopScanning);

            this.sink.Devices.CollectionChanged += displaysChanged;
        }

        private void displaysChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(Devices));
        }

        private async void StartScanning(object _)
        {
            await this.sink.StartScanning();
            this.scanningState = true;
        }

        private async void StopScanning(object _)
        {
            await this.sink.StopScanning();
            this.scanningState = false;
        }

        private bool _Connected;
        public bool Connected
        {
            get { return _Connected; }
            set
            {
                _Connected = value;
                this.scanningState = false;
                this.OnPropertyChanged();
                this.OnPropertyChanged(nameof(CanChangeAddress));
                this.OnPropertyChanged(nameof(CanControlScanning));
                this.OnPropertyChanged(nameof(CanDisconnect));


                this.OnPropertyChanged(nameof(ShowStopScaning));
                this.OnPropertyChanged(nameof(ShowStartScaning));
            }
        }

        public bool CanChangeAddress
        {
            get
            {
                return !this.Connected;
            }
        }

        public bool CanControlScanning
        {
            get
            {
                return this.Connected;
            }
        }

        public bool CanDisconnect
        {
            get
            {
                return this.Connected;
            }
        }

        private bool scanningState = false;

        public Visibility ShowStartScaning
        {
            get
            {
                return (!this.scanningState) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ShowStopScaning
        {
            get
            {
                return this.scanningState ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public ICommand StartScanningCommand
        {
            get;
        }

        public ICommand StopScanningCommand
        {
            get;
        }
        
        public IEnumerable<ButtplugSinkDevice> Devices
        { 
            get
            {
                var ret = new ButtplugSinkDevice[this.sink.Devices.Count];
                this.sink.Devices.CopyTo(ret,0);

                return ret;
            }
        }

        public string ServerUrl
        {
            get { return this.sink.ServerAddress; }
            set
            {
                this.sink.ServerAddress = value;
                this.OnPropertyChanged();
            }
        }
    }
}
